# Fenix Installer
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)
# 2. 

from typing import TypedDict

class Main:
    """
    TODO
    ----
    Implement a command-line UI
    """

    def __init__(self, commandline_arguments: TypedDict):
        if commandline_arguments.mode in (
            'silent_minimal',
            'minimal',
            'silent_basic',
            'basic',
            'silent_full',
            'full'
        ):
            pass
        else:
            print("This mode is not implemented on command-line interface interface yet.")
            exit(1)

