# Fenix Installer
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# GUI
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# ARTWORK
# 1. Trivoxel (https://gitlab.com/TriVoxel)

# FENIX IMPORTS
from fenix_library.configuration import JSONConfiguration
from user_interface.gtk.utilities.page import FenixInstallerPage, PageTools
from user_interface.gtk.utilities.image import ImageTools
from user_interface.gtk.utilities.text import TextTools

# CUSTOM IMPORTS
import gi # Python GObject introspection module which contains Python bindings and support for Gtk
gi.require_version('Gtk', '3.0') # make sure that the Gtk version is at the required level
from gi.repository import Gtk # Gtk related modules for the graphical interface
from gi.repository import GdkPixbuf # Gtk module that describes images
from gi.repository import GLib # for using a function that will allow updating the on-screen console text

# Unique name for use in configuration files for looking up details about this page
# ----------- Modify this ---------- #
CURRENT_PAGE_NAME = "welcome"
# ----------- Modify this ---------- #

class Page(FenixInstallerPage):
# create a page class derived from "FenixInstallerPage" which contains a page template

    # CONSTRUCTOR
    def __init__(self):
        super().__init__(CURRENT_PAGE_NAME) # call the super-class constructor that handles all the tasks associated with page creation automatically
        
        # ---------- Custom code ----------- #
        self.bigLogo            = self.builder.get_object("bigLogo")
        self.logo_filepath = "media/branding/RebornOS_Logo_1.svg"
        self.pixbuf             = ImageTools.getPixbufFromFile(self.logo_filepath)
        self.bigLogoAspectRatio = float(self.pixbuf.get_width()) / self.pixbuf.get_height()
        # -------- Custom code ends -------- #

    # EVENT HANDLERS
    # To specify how this particular Gtk container handles user interactions. The names of handler functions can be assigned in `Glade` under "Signals"
    # Create functions (with names as described above) that are called when specific UI events take place
    def welcome_onRebornResize(self, box, alloc):
        # Called when the box containing the Reborn logo is allocated a new size
        width  = box.get_allocated_width()
        height = box.get_allocated_height()
        if float(width)/height > self.bigLogoAspectRatio:
            # pixbuf = ImageTools.scalePixbufByHeight(self.pixbuf, height-5, self.bigLogoAspectRatio)
            pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(
                filename= self.logo_filepath,
                width= -1,
                height= height - 10
            )
        else:
            # pixbuf = ImageTools.scalePixbufByWidth(self.pixbuf, width-5, self.bigLogoAspectRatio)
            pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(
                filename = self.logo_filepath,
                width= width - 10,
                height= -1
            )
        ImageTools.addImageFromPixbuf(pixbuf, self.bigLogo)
        box.queue_draw()
        
    # CUSTOM METHODS

