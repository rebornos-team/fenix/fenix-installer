# Fenix Installer
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)
# 2. 

# GUI
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)
# 2. 

# IMPORTS
import os # for filepath related methods
import gi # Python GObject introspection module which contains Python bindings and support for Gtk
gi.require_version('Gtk', '3.0') # make sure that the Gtk version is at the required level
from gi.repository import Gtk, GLib # Gtk related modules for the graphical interface
from argparse import Namespace
from typing import List
from pathlib import Path
import logging

# FENIX IMPORTS
from fenix_library.configuration import JSONConfiguration
from fenix_library.running import LoggingHandler, LogMessage, Command
from user_interface.gtk.utilities.page import FenixInstallerPage, PageTools

logger = logging.getLogger('fenix_installer.ui.gtk.code'+'.'+ Path(__file__).stem)

# THE EVENT HANDLER
class Main:  
    """
    Specify how this particular Gtk container handles user interaction events. 
    The names of handler functions (also called `signals` in Gtk) can be assigned in `Glade` under "Signals

    TODO
    ----
    - Splash screen before loading the pages and the main window, to avoid delay in getting some UI up for the user

    """

    def __init__(self, commandline_arguments: Namespace) -> None:
        """
        Initialize the main installer window in Gtk

        Parameters
        ----------
        commandline_arguments: Namespace
            Contains the command line arguments
        """

        self.logging_handler = LoggingHandler(logger=logger)
        LogMessage.Info("Creating a Gtk Builder and importing the UI from glade files...").write(self.logging_handler)
        self.builder = Gtk.Builder()
        self.builder.add_from_file( # extract the main form from the glade file
            os.path.join(
                "user_interface",
                commandline_arguments.user_interface,
                "forms",
                "main.glade"
            )
        ) 
        self.builder.connect_signals(self) # connect the signals from the Gtk forms to our event handlers (which are all defined in a class)
        FenixInstallerPage.main_builder = self.builder
        FenixInstallerPage.console_buffer = self.builder.get_object("consoleTextView").get_buffer() # get access to the console buffer

        page_stack: Gtk.Stack = self.builder.get_object("main.box.paned.stack") # get the installer's Gtk Stack. This stack stores the "pages" of our installer and shows them one at a time    
        page_configuration: JSONConfiguration = JSONConfiguration("configuration/pages.json")
        list_of_pages: List[str] = page_configuration["initial_pages"] + page_configuration["mode_templates"][commandline_arguments.mode]
        LogMessage.Info("Loading initial pages...").write(self.logging_handler)
        PageTools.add_pages(
            list_of_pages,
            page_stack,
            page_configuration
        )
        page_configuration["added_pages"] = [page_name for page_name in page_configuration["added_pages"] if page_name not in page_configuration["initial_pages"]] # Do not store initial common pages as added pages, in order to prevent modifying them when the mode is changed
        page_configuration.write_data()
        
        self.installer_settings = JSONConfiguration("configuration/installer.json")
        self.installer_settings["installing"] = False
        self.installer_settings.write_data()

        LogMessage.Info("Displaying the main window...").write(self.logging_handler)
        self.builder.get_object("main").show_all() # get the main form object and make it visible

        LogMessage.Info("Preloading enabled. Loading all pages...").write(self.logging_handler)
        if self.installer_settings["preload_pages"]: # preload pages if set to do so
            GLib.idle_add(
                PageTools.preload_pages,
                page_configuration
            )
        LogMessage.Info("Starting the event loop...").write(self.logging_handler)
        Gtk.main() # start the GUI event loop

    def main_onClose(self, _):

        """
        Quit the graphical interface

        Called when the application is closedMainFormHandler
        This happens either when
        (1) the container object is destroyed or 
        (2) when the `Abort` button is clicked
        The arguments received in this method may have been selected inside `Glade` under the corresponding signal
        """

        if (JSONConfiguration("configuration/installer.json")["installing"]
            and FenixInstallerPage.installer_job is not None) : # If the installer job is running
            FenixInstallerPage.installer_job.abort() # Send a request to abort the installation
        Gtk.main_quit() # Quit from the Gtk UI

    def main_onBackClicked (self, page_stack: Gtk.Stack):
        """If the installation has not started yet, switch to the previous page

        Called when either 
        (1) the `Back` button is clicked or
        (2) the `back icon` of the headerbar is clicked
        The arguments received in this method may have been selected inside `Glade` under the corresponding signal"""

        page_configuration = JSONConfiguration("configuration/pages.json") # read the configuration file for data related to pages
        if not JSONConfiguration("configuration/installer.json")["installing"]: # if the installation has not started yet
            list_of_page_names = page_configuration["initial_pages"] + page_configuration["added_pages"]
            current_page_index = list_of_page_names.index(page_stack.get_visible_child_name()) # Determine the index of the current visible page
            if current_page_index > 0: # If the currently visible page is not the first one
                page_stack.set_visible_child_name(list_of_page_names[current_page_index - 1]) # Select the previous page name from the list and make it visible

    def main_onNextClicked (self, page_stack: Gtk.Stack): 
        
        """ If the installation has not started yet, switch to the next page
        
        Called when either 
        (1) the `Next` button is clicked or
        (2) the `Next icon` of the headerbar is clicked     
        The arguments received in this method may have been selected inside `Glade` under the corresponding signal"""
              
        page_configuration = JSONConfiguration("configuration/pages.json") # read the configuration file for data related to pages     
        if not JSONConfiguration("configuration/installer.json")["installing"]: # if the installation has not started yet
            list_of_page_names = page_configuration["initial_pages"] + page_configuration["added_pages"]
            current_page_index = list_of_page_names.index(page_stack.get_visible_child_name()) # Determine the index of the current visible page
            if current_page_index < (len(list_of_page_names) - 1): # If the currently visible page is not the last one
                page_stack.set_visible_child_name(list_of_page_names[current_page_index + 1])  # Select the next page name from the list and make it visible

    def main_consoleResized(self, console_scrolled_window: Gtk.ScrolledWindow, _):

        """ Scroll down the GUI based debugging console as new text is added and the console changes in size

        Called by Gtk when the debugging console changes in size because of new text
        The arguments received in this method may have been selected inside `Glade` under the corresponding signal"""

        adjustment = console_scrolled_window.get_vadjustment()
        adjustment.set_value(adjustment.get_upper() - adjustment.get_page_size())
        console_scrolled_window.set_vadjustment(adjustment) # scroll to the bottom of the debugging console

    def on_about_clicked(self, _):
        LogMessage.Debug("Bringing up the \"About\" dialog...").write(self.logging_handler)
        self.builder.get_object("about").show_all()

    def on_log_clicked(self, _):
        LogMessage.Debug("Opening the log on the default editor...").write(self.logging_handler)
        command = Command(["xdg-open", self.installer_settings["current_log_file_path"]])
        command.run_and_log(self.logging_handler)

    def on_config_clicked(self, _):
        LogMessage.Debug("Opening the installer configuration file on the default editor...").write(self.logging_handler)
        command = Command(["xdg-open", "configuration/installer.json"])
        command.run_and_log(self.logging_handler)

    def on_other_config_clicked(self, _):
        LogMessage.Debug("Opening the configuration directory on the default file manager...").write(self.logging_handler)
        command = Command(["xdg-open", "configuration/"])
        command.run_and_log(self.logging_handler)

    def on_about_close(self, _):
        LogMessage.Debug("Hiding the \"About\" dialog...").write(self.logging_handler)
        self.builder.get_object("about").hide()
