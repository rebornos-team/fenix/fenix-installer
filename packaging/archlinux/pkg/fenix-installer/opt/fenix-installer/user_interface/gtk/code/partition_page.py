# Fenix Installer
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# GUI
# 1. Trivoxel (https://gitlab.com/TriVoxel)

# FENIX IMPORTS
from fenix_library.configuration import JSONConfiguration
from user_interface.gtk.utilities.page import FenixInstallerPage, PageTools
from user_interface.gtk.utilities.image import ImageTools
from user_interface.gtk.utilities.text import TextTools

# CUSTOM IMPORTS
import psutil
import gi # Gtk related modules
gi.require_version('Gtk', '3.0')   
from gi.repository import Gtk, GdkPixbuf, GLib, Pango

# Unique name for use in configuration files for looking up details about this page
# ----------- Modify this ---------- #
CURRENT_PAGE_NAME = "partition"
# ----------- Modify this ---------- #

class Page(FenixInstallerPage):
# create a page class derived from "FenixInstallerPage" which contains a page template

    # CONSTRUCTOR
    def __init__(self):
        super().__init__(CURRENT_PAGE_NAME) # call the super-class constructor that handles all the tasks associated with page creation automatically
        
        # ---------- Custom code ----------- #
        self.refreshPartitionList()
        # -------- Custom code ends -------- #

    # EVENT HANDLERS
    # To specify how this particular Gtk container handles user interactions. The names of handler functions can be assigned in `Glade` under "Signals"
    # Create functions (with names as described above) that are called when specific UI events take place


    # CUSTOM METHODS
    def refreshPartitionList(self):
        self.partition_treeView = self.builder.get_object("partition_treeView")
        self.liststore = Gtk.ListStore(str, str, str, bool, str, str, str)
        self.partition_treeView.set_model(self.liststore)
       
        self.partition_treeView.append_column(Gtk.TreeViewColumn("Device", Gtk.CellRendererText(), text=0))
        self.partition_treeView.append_column(Gtk.TreeViewColumn("Type", Gtk.CellRendererText(), text=0))
        self.partition_treeView.append_column(Gtk.TreeViewColumn("Mount Point", Gtk.CellRendererText(), text=0))
        self.partition_treeView.append_column(Gtk.TreeViewColumn("Format?", Gtk.CellRendererText(), text=0))
        self.partition_treeView.append_column(Gtk.TreeViewColumn("Size", Gtk.CellRendererText(), text=0))
        self.partition_treeView.append_column(Gtk.TreeViewColumn("Used", Gtk.CellRendererText(), text=0))
        self.partition_treeView.append_column(Gtk.TreeViewColumn("System", Gtk.CellRendererText(), text=0))
