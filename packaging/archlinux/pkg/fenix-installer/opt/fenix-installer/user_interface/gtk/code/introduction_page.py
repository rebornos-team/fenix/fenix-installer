# Fenix Installer
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# GUI
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# ARTWORK
# 1. Trivoxel (https://gitlab.com/TriVoxel)

# FENIX IMPORTS
from fenix_library.configuration import JSONConfiguration
from user_interface.gtk.utilities.page import FenixInstallerPage, PageTools
from user_interface.gtk.utilities.image import ImageTools
from user_interface.gtk.utilities.text import TextTools

# CUSTOM IMPORTS
import json                                              # for reading configuration files
import gi                                                #
gi.require_version('Gtk', '3.0')                         #
from gi.repository import Gtk, Pango                     # for text tags
# from gi.repository import WebKit2, WebKit2WebExtension

# Unique name for use in configuration files for looking up details about this page
# ----------- Modify this ---------- #
CURRENT_PAGE_NAME = "introduction"
# ----------- Modify this ---------- #

class Page(FenixInstallerPage):
# create a page class derived from "FenixInstallerPage" which contains a page template

    # CONSTRUCTOR
    def __init__(self):
        super().__init__(CURRENT_PAGE_NAME) # call the super-class constructor that handles all the tasks associated with page creation automatically
        
        # ---------- Custom code ----------- #
        self.addIntroduction()                   
        # -------- Custom code ends -------- #

    # EVENT HANDLERS
    # To specify how this particular Gtk container handles user interactions. The names of handler functions can be assigned in `Glade` under "Signals"
    # Create functions (with names as described above) that are called when specific UI events take place
    

    # CUSTOM METHODS
    def addIntroduction(self):
        introductionTextBuffer = self.builder.get_object("introduction_TextView").get_buffer()
        TextTools.addBasicTextTags(introductionTextBuffer)
        iter = introductionTextBuffer.get_start_iter()
        introductionTextBuffer.insert_with_tags_by_name(iter, "Choose From Several Desktop Environments:", "bold")
        introductionTextBuffer.insert_with_tags_by_name(iter, "\nOne ISO with a myriad of Desktop Environments to choose from! Whether you are a fan of KDE, GNOME, Deepin, Budgie, OpenBox, i3, or Xfce - RebornOS is for you. For those who love the terminal, there is even the option of a Minimal install, allowing you to bypass the Desktop Environment entirely. RebornOS gets out of the way and lets you decide, not the other way around.")
        introductionTextBuffer.insert_with_tags_by_name(iter, "\n\nCustomize it to your heart's content", "bold")
        introductionTextBuffer.insert_with_tags_by_name(iter, "\nWith a lot of different options in the installer, RebornOS truly caters to your needs. Whatever they may be.")               
        introductionTextBuffer.insert_with_tags_by_name(iter, "\n\nNever Fall Behind:", "bold")
        introductionTextBuffer.insert_with_tags_by_name(iter, "\nRebornOS is based on both Arch Linux, meaning that it features a rolling release model. Due to this, you will only ever have to install once - and never need to, again. Enjoy getting the latest and the best from the Linux community, all available in an easy-to-use software center.")
        introductionTextBuffer.insert_with_tags_by_name(iter, "\n\nPrivate and Secure:", "bold")
        introductionTextBuffer.insert_with_tags_by_name(iter, "\nRebornOS is based on Linux, which is known to be private and secure. The code can be analyzed and vetted by third parties, unlike that of other closed source operating systems.")
        introductionTextBuffer.insert_with_tags_by_name(iter, "\n\nBuilt For You - By You:", "bold")
        introductionTextBuffer.insert_with_tags_by_name(iter, "\nRebornOS truly puts our users first, implementing your suggestions o the extent possible.") 
