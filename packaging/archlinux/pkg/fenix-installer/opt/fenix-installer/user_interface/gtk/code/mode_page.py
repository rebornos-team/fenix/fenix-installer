# Fenix Installer
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# GUI
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# FENIX IMPORTS
from fenix_library.configuration import JSONConfiguration
from user_interface.gtk.utilities.page import FenixInstallerPage, PageTools
from user_interface.gtk.utilities.image import ImageTools
from user_interface.gtk.utilities.text import TextTools

# CUSTOM IMPORTS
from collections import Counter
import concurrent.futures
from gi.repository import GLib

# Unique name for use in configuration files for looking up details about this page
# ----------- Modify this ---------- #
CURRENT_PAGE_NAME = "mode"
# ----------- Modify this ---------- #

class Page(FenixInstallerPage):
# create a page class derived from "FenixInstallerPage" which contains a page template

    # CONSTRUCTOR
    def __init__(self):
        super().__init__(CURRENT_PAGE_NAME) # call the super-class constructor that handles all the tasks associated with page creation automatically

        # ---------- Custom code ----------- #
        mode_name = JSONConfiguration("configuration/installer.json").get_current_choice_for_item("modes")
        self.builder.get_object("mode_" + mode_name + "_button").set_active(True)
        # -------- Custom code ends -------- #

    # EVENT HANDLERS
    # To specify how this particular Gtk container handles user interactions. The names of handler functions can be assigned in `Glade` under "Signals"
    # Create functions (with names as described above) that are called when specific UI events take place
    def mode_onModeSelected(self, button):
        if button.get_active():  
            mode_name = button.get_label().lower().replace("-", "_")
            installer_configuration = JSONConfiguration("configuration/installer.json")
            installer_configuration.set_current_choice_for_item("modes", mode_name)
            installer_configuration.write_data()
            page_configuration = JSONConfiguration("configuration/pages.json")
            list_of_pages = page_configuration["mode_templates"][mode_name]

            if Counter(page_configuration["added_pages"]) == Counter(list_of_pages): # Check if the lists have the same elements
                return None # Do nothing and exit
            else: 
                GLib.idle_add( # Add the task of page updating to the Gtk idle thread
                    self._page_update_worker,
                    mode_name,
                    page_configuration,
                    list_of_pages
                )

    # CUSTOM METHODS

    def _page_update_worker(self, mode_name, page_configuration, list_of_pages):
        page_stack = FenixInstallerPage.main_builder.get_object("main.box.paned.stack") # get the installer's Gtk Stack. This stack stores the "pages" of our installer and shows them one at a time
        PageTools.remove_pages(page_configuration["added_pages"], page_stack, page_configuration)
        PageTools.add_pages(list_of_pages, page_stack, page_configuration)
        PageTools.refresh_pages(page_stack)
        page_configuration.write_data()

        installer_settings = JSONConfiguration("configuration/installer.json")
        installer_settings.set_current_choice_for_item("modes", mode_name)
        installer_settings.write_data()

