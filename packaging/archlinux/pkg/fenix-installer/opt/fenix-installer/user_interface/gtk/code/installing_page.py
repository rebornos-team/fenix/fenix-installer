# Fenix Installer
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)
# 2. 

# GUI
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)
# 2. 

# ARTWORK
# 1. Trivoxel (https://gitlab.com/TriVoxel)

# CUSTOM IMPORTS
import logging
from pathlib import Path
import threading                                       # for multithreading
import gi                                              # Gtk related modules
gi.require_version('Gtk', '3.0')                       #       
from gi.repository import Gtk                          #
import functools

# FENIX IMPORTS
import scripts.install
from fenix_library.configuration import JSONConfiguration
from fenix_library.running import LoggingHandler
from user_interface.gtk.utilities.page import FenixInstallerPage, PageTools
from user_interface.gtk.utilities.image import ImageTools
from user_interface.gtk.utilities.text import TextTools

logger = logging.getLogger('fenix_installer'+'.'+ Path(__file__).stem)

# Unique name for use in configuration files for looking up details about this page
# ----------- Modify this ---------- #
CURRENT_PAGE_NAME = "installing"
# ----------- Modify this ---------- #

class Page(FenixInstallerPage):
# create a page class derived from "FenixInstallerPage" which contains a page template

    # CONSTRUCTOR
    def __init__(self):
        super().__init__(CURRENT_PAGE_NAME) # call the super-class constructor that handles all the tasks associated with page creation automatically

        # ---------- Custom code ----------- #
        stack = self.builder.get_object("installing_slideStack")
        self.slidePaths   = [
            "media/slides/Slide 1 Full No Border.jpg",
            "media/slides/Slide 2 Full No Border.jpg",
            "media/slides/Slide 3 Full No Border.jpg"
        ]
        self.slideImages  = []
        self.slidePixbufs = []
        self.aspectRatios = []
        for slidePath in self.slidePaths:            
            scrolledWindow = Gtk.ScrolledWindow.new(None, None)
            viewport = Gtk.Viewport.new(None, None)
            image = Gtk.Image.new()
            stack.add_named(scrolledWindow, slidePath)
            scrolledWindow.add(viewport)
            viewport.add(image)
            # FenixInstallerPage.addImage(slidePath, 200, image) 
            pixbuf = ImageTools.getPixbufFromFile(slidePath)
            aspectRatio = float(pixbuf.get_width()) / pixbuf.get_height()
            self.slideImages.append(image)
            self.slidePixbufs.append(pixbuf)
            self.aspectRatios.append(aspectRatio)
        # -------- Custom code ends -------- #

    # EVENT HANDLERS
    # To specify how this particular Gtk container handles user interactions. The names of handler functions can be assigned in `Glade` under "Signals"
    # Create functions (with names as described above) that are called when specific UI events take place
    def installing_onSlideResized(self, stack, rectangle):
        width  = stack.get_allocated_width()
        height = stack.get_allocated_height()
        for i in range(len(self.slidePaths)):
            if float(width)/height > self.aspectRatios[i]:
                pixbuf = ImageTools.scalePixbufByHeight(self.slidePixbufs[i], height-75, self.aspectRatios[i])
            else:
                pixbuf = ImageTools.scalePixbufByWidth(self.slidePixbufs[i], width-75, self.aspectRatios[i])
            ImageTools.addImageFromPixbuf(pixbuf, self.slideImages[i])

    def installing_onLeftClicked(self, stack):
        currentSlideIndex = self.slidePaths.index(stack.get_visible_child_name())   # Determine the index of the current visible slide
        if currentSlideIndex > 0:                                                   # If the currently visible slide is not the first one
            stack.set_visible_child_name(self.slidePaths[currentSlideIndex - 1])    # Select the previous slide name from the list and make it visible
        else:                                                                       # otherwise
            stack.set_visible_child_name(self.slidePaths[len(self.slidePaths) - 1]) # select the last slide name from the list and make it visible

    def installing_onRightClicked(self, stack):
        currentSlideIndex = self.slidePaths.index(stack.get_visible_child_name()) # Determine the index of the current visible slide
        if currentSlideIndex < (len(self.slidePaths) - 1):                        # If the currently visible slide is not the last one
            stack.set_visible_child_name(self.slidePaths[currentSlideIndex + 1])  # select the next slide name from the list and make it visible
        else:                                                                     # otherwise
            stack.set_visible_child_name(self.slidePaths[0])                      # select the first slide name from the list and make it visible

    def installing_onInstallClicked(self, button):
        dialog = self.builder.get_object("installConfirmationDialog") # 
        dialog.show_all()                                             # show the installation confirmation dialog

    def installing_onNoClicked(self, dialog):
        dialog.hide()                         # when 'no' is clicked after the user is prompted to confirm installation

    def installing_onInstallConfirmed(self, dialog):
        dialog.hide()                                                                  # hide the installation confirmation dialog
        self.builder.get_object("installing_stack").set_visible_child_name("progress") # switch to installation mode where the page shows progress bar and slides        
        self.startInstallation() # start the installation
        # threading.Thread(target=self.runDuringInstallation).start() # start the installation on a thread separate from the GUI

    # CUSTOM METHODS
    def run_before_installation(self):
        installer_settings = JSONConfiguration("configuration/installer.json")
        installer_settings["installing"] = True # set the configuration variable to indicate that installation is in progress
        installer_settings.write_data()

        FenixInstallerPage.main_builder.get_object("main.box.paned.stacksidebar").set_sensitive(False)
        FenixInstallerPage.main_builder.get_object("main.box.bottomBar.nextButton").set_sensitive(False)
        FenixInstallerPage.main_builder.get_object("main.box.bottomBar.backButton").set_sensitive(False)
        FenixInstallerPage.main_builder.get_object("main_topBackButton").set_sensitive(False)
        FenixInstallerPage.main_builder.get_object("main_topNextButton").set_sensitive(False)

        # self.openLogFile() # create and open a new log file (OLD way!!!!!)

    def run_after_installation(self):
        # self.closeLogFile() # (OLD way!!!!!)

        FenixInstallerPage.main_builder.get_object("main.box.paned.stacksidebar").set_sensitive(True)
        FenixInstallerPage.main_builder.get_object("main.box.bottomBar.nextButton").set_sensitive(True)
        FenixInstallerPage.main_builder.get_object("main.box.bottomBar.backButton").set_sensitive(False)
        FenixInstallerPage.main_builder.get_object("main_topBackButton").set_sensitive(False)
        FenixInstallerPage.main_builder.get_object("main_topNextButton").set_sensitive(True)
        FenixInstallerPage.main_builder.get_object("main.box.bottomBar.abortButton").set_label("Finish")
        FenixInstallerPage.main_builder.get_object("main.box.paned.stack").set_visible_child_name("finish")

        installer_settings = JSONConfiguration("configuration/installer.json")
        installer_settings["installing"] = False # set the configuration variable to indicate that installation is not in progress
        installer_settings.write_data() 

    def startInstallation(self):
        logging_handler = LoggingHandler(
            logger=logger,
            logging_functions= [self.append_to_console_text]
        )
        FenixInstallerPage.installer_job = scripts.install.install(
            logging_handler= logging_handler,
            pre_install_function= functools.partial(self.run_before_installation),
            post_install_function= functools.partial(self.run_after_installation)
        )
            