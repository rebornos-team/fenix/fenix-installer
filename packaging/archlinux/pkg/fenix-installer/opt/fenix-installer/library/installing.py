# Fenix Library
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/libraries

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)
# 2. 

# IMPORTS
from __future__ import annotations
from abc import ABC
import textwrap
import functools
import fileinput
from pathlib import Path
from typing import List, Tuple, Callable, Union, Optional
from enum import Enum

# FENIX IMPORTS
from fenix_library.running import BatchJob, LogMessage, Command, Function, LoggingHandler
from fenix_library.configuration import JSONConfiguration
from library.analyzing import SystemAnalyzer

class AbstractInstaller(BatchJob, ABC):
    """
    Represents an installer for installing an operating system

    Attributes
    ----------
    self.installer_settings: JSONConfiguration
        Settings related to the installer
    self.system_settings: JSONConfiguration
        Settings related to the host system
    self.selected_options: JSONConfiguration
        Settings common across all modes
    self.mode_specific_options: JSONConfiguration
        Settings pertaining to the chosen installation mode
    SILENT_MINIMAL: InstallationMode
        Denotes the silent minimal mode
    SILENT_BASIC: InstallationMode
        Denotes the silent basic mode
    SILENT_FULL: InstallationMode
        Denotes the silent full mode
    MINIMAL: InstallationMode
        Denotes the minimal mode
    BASIC: InstallationMode
        Denotes the basic mode
    FULL: InstallationMode
        Denotes the full mode
    SEMI_AUTOMATIC: InstallationMode
        Denotes the semi-automatic mode
    CUSTOMIZED: InstallationMode
        Denotes the customized mode

    """  

    class InstallationMode(Enum):
        """
        Stores the installation mode

        """

        SILENT_MINIMAL = 1,
        SILENT_BASIC = 2,
        SILENT_FULL = 3,
        MINIMAL = 4,
        BASIC = 5,
        FULL = 6,
        SEMI_AUTOMATIC = 7,
        CUSTOMIZED = 8

    SILENT_MINIMAL = InstallationMode.SILENT_MINIMAL
    SILENT_BASIC = InstallationMode.SILENT_BASIC
    SILENT_FULL = InstallationMode.SILENT_FULL
    MINIMAL = InstallationMode.MINIMAL
    BASIC = InstallationMode.BASIC
    FULL = InstallationMode.FULL
    SEMI_AUTOMATIC = InstallationMode.SEMI_AUTOMATIC
    CUSTOMIZED = InstallationMode.CUSTOMIZED

    # CONSTRUCTOR

    def __init__(
        self,
        thread_name:str= "InstallJob",
        logging_handler: LoggingHandler = None,
        *, # The arguments following this must all have a name. For example, function(a= 1, text= "hello")
        pre_run_function: Optional[functools.partial] = None,
        post_run_function: Optional[functools.partial] = None
    ) -> None:
        """
        Initialize the current instance
        
        Parameters
        ----------
        thread_name: str
            Name for prefixing the threads
        logging_handler: Optional[LoggingHandler]
            The LoggingHandler object which stores the logging functions, logging threads, logger information, etc.
        pre_run_function: functools.partial, default None
            A function to be called before the installation starts. Its name and arguments are wrapped together by calling functools.partial
        post_run_function: functools.partial, default None
            A function to be called after the installation finishes. Its name and arguments are wrapped together by calling functools.partial
        """

        # Get configuration files
        self.installer_settings: JSONConfiguration = JSONConfiguration("configuration/installer.json")
        self.system_settings: JSONConfiguration = JSONConfiguration("configuration/system.json")
        self.selected_options: JSONConfiguration = JSONConfiguration("configuration/user_selected/common_options.json")

        mode_name: str = self.installer_settings.get_current_choice_for_item("modes").replace('-', '_')
        self.mode_specific_options: JSONConfiguration = JSONConfiguration("configuration/user_selected/" + mode_name + "_mode_options.json")

        # TODO: This command gives the wrong mode.
        self.installation_mode: AbstractInstaller.InstallationMode = AbstractInstaller.InstallationMode[mode_name.upper()]
        
        # Set up a system analyzer
        self.system_analyzer = SystemAnalyzer(logging_handler= logging_handler)

        # Call the parent class constructor        
        BatchJob.__init__(
            self,
            thread_name= thread_name,
            logging_handler= logging_handler,
            pre_run_function= pre_run_function,
            post_run_function= post_run_function
        )

    # PRIVATE METHODS

    def process_system_analysis(self) -> bool:
        """
        Checks the configuration files for the results of system analysis and determines if the host system satisfies the requirements for the installation

        Parameters
        ----------
        None

        Returns
        -------
        requirements_met: bool
            True if the system meets requirements for installation
            False if the system does not meet requirements for installation
        """

        requirements_met: bool = False

        if (not self.system_settings["power"]["plugged_in"]) and self.system_settings["power"]["charge"] < 60:
            LogMessage.Critical(message= "Please plug in your device to a power source... Fenix cannot proceed...").write(logging_handler= self.logging_handler)
            LogMessage.Info(message= "Fenix encountered a critical error. The installation did not complete." ).write(logging_handler= self.logging_handler)
            print("Fenix encountered a critical error. The installation did not complete...")
        elif not self.system_settings["internet"]["connected"]: 
            if self.installer_settings["package_cache_directory"] == "":
                LogMessage.Critical(message= "Unable to connect to the internet... Fenix cannot proceed...")
                LogMessage.Info(message= "Fenix encountered a critical error. The installation did not complete...")
                print("Fenix encountered a critical error. The installation did not complete...")
            else:
                requirements_met = True
        else:
            requirements_met = True
        
        if not requirements_met:
            self.abort() 

        return requirements_met

class ArchLinuxInstaller(AbstractInstaller):
    """
    Handy tools for installing ArchLinux

    TODO
    ----
    - Installation modes
    - Pre-install checking for basic requirements
    - Pre-install checking whether all options are selected
    - Failsafe for aborting on error
    """

    # CONSTRUCTOR

    def __init__(
        self,
        thread_name:str= "ArchLinuxInstallJob",
        logging_handler: Optional[LoggingHandler] = None,
        pre_run_function: Optional[functools.partial] = None,
        post_run_function: Optional[functools.partial] = None
    ) -> None:
        """
        Initialize an 'ArchLinuxInstaller' object
        
        Parameters
        ----------
        thread_name: str
            Name for prefixing the threads
        logging_handler: Optional[LoggingHandler]
            The LoggingHandler object which stores the logging functions, logging threads, logger information, etc.
        pre_run_function: functools.partial, default None
            A function to be called before the installation starts. Its name and arguments are wrapped together by calling functools.partial
        post_run_function: functools.partial, default None
            A function to be called after the installation finishes. Its name and arguments are wrapped together by calling functools.partial
        """

        super().__init__(
            thread_name= thread_name,
            logging_handler= logging_handler,
            pre_run_function= pre_run_function,
            post_run_function= post_run_function
        )

    # REGULAR METHODS

    def start(self) -> None:
        """
        Starts the installation process based on the mode of installation as specified by the configuration files

        Parameters
        ----------
        None

        Returns
        -------
        Nothing
        """

        self += self.system_analyzer
        self += Function(self.process_system_analysis)
        self += LogMessage.Info("Starting installation in \"" + str(self.installation_mode).upper() + "\" mode...")

        # Queue the installation
        self.queue_installation()
        # Queue a log message (will not display yet)
        self += LogMessage.Info("The installer is done with its tasks... Please check the log for errors and warnings...")

        super().start()
      
    def queue_partitioning(self):
        self += LogMessage.Info("Partitioning...")
        self += Command.Script("scripts/test/test_partition.sh")

    def queue_package_caching(self):        
        if (self.installer_settings["package_cache_directory"] is not None) and (self.installer_settings["package_cache_directory"] != ""):
            self += LogMessage.Info("Copying packages from external cache at " + self.installer_settings["package_cache_directory"] + "...")
            self += Command(["mkdir", "-p", "/install/var/cache/pacman/pkg"])
            self += Command(["mkdir", "-p", "/install/var/lib/pacman/sync/"])
            self += Command.Shell("cp -r -f " + self.installer_settings["package_cache_directory"] + "/*.db /install/var/lib/pacman/sync/")
            self += Command.Shell("cp -r -f " + self.installer_settings["package_cache_directory"] + "/*.pkg.tar* /install/var/cache/pacman/pkg/")
        
        if self.installer_settings["offline"]:
            self += LogMessage.Info("Tweaking pacstrap to allow offline installation...")
            self += Command(["sed", "-i", "s/pacmode=-Sy/pacmode=-S/g", "/usr/bin/pacstrap"])

    def queue_mirror_setup(self):
        self += LogMessage.Info("Selecting mirrors...")
        self += Command(["reflector", "--latest", "25", "--protocol", "https", "--sort", "rate", "--save", "/etc/pacman.d/mirrorlist"])

    def queue_base_installation(self):
        self += LogMessage.Info("Bootstrapping with the base ArchLinux packages...")
        self += Command(["pacstrap", "/install", "base"])

    def queue_kernel_installation(self):
        self += LogMessage.Info("Installing the kernels...")
        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S"] + self.selected_options["kernels"]) 

    def queue_firmware_installation(self):
        self += LogMessage.Info("Installing firmware...")
        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S", "linux-firmware"])

    def queue_genfstab(self):
        self += LogMessage.Info("Generating fstab...")
        self += Command.Shell(" ".join(["genfstab", "-L", "/install", ">>" , "/install/etc/fstab"]))

    def queue_setting_timezone(self):
        self += LogMessage.Info("Setting the Time Zone...")
        self += Command(["arch-chroot" ,"/install", "ln", "-sf", "/usr/share/zoneinfo/" + self.selected_options["timezone"]["region"] + "/" + self.selected_options["timezone"]["city"], "/etc/localtime"])
        self += Command(["arch-chroot", "/install", "hwclock", "--systohc"]) 

    def queue_setting_locale(self):
        self += LogMessage.Info("Generating Locale...")
        self += Command(["arch-chroot", "/install", "echo", "en_US.UTF-8 UTF-8"]) >> Path("/install/etc/locale.gen")
        self += Command(["arch-chroot", "/install", "locale-gen"])
        self += Command(["arch-chroot", "/install", "echo", "LANG=en_US.UTF-8"]) > Path("/install/etc/locale.conf") 
        self += Command(["arch-chroot", "/install", "export", "LANG=en_US.UTF-8"])  

    def queue_setting_hostname(self):
        self += LogMessage.Info("Setting hostname...")
        self += Command(["arch-chroot", "/install", "echo", self.selected_options["hostname"]]) > Path("/install/etc/hostname")

    def queue_setting_network_connectivity(self):
        self += LogMessage.Info("Enabling network connectivity...")
        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S", "networkmanager"]) 
        self += Command(["arch-chroot", "/install", "systemctl", "enable", "NetworkManager"])   

    def queue_setting_bootloader(self):
        self += LogMessage.Info("Setting up Grub...")        
        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S", "grub", "efibootmgr"]) 
        self += Command(["arch-chroot", "/install", "grub-install", "--target=i386-pc", "/dev/sda"])
        self += Command(["arch-chroot", "/install", "grub-mkconfig", "-o", "/boot/grub/grub.cfg"])

    def queue_installing_desktops(self):
        if self.installation_mode not in [ArchLinuxInstaller.MINIMAL, ArchLinuxInstaller.SILENT_MINIMAL]:
            if self.selected_options["desktops"]["gnome"]["enabled"]:
                self.queue_installing_gnome()
            if self.selected_options["desktops"]["plasma"]["enabled"]:
                self.queue_installing_plasma()
            if self.selected_options["desktops"]["cinnamon"]["enabled"]:
                self.queue_installing_cinnamon()
            if self.selected_options["desktops"]["budgie"]["enabled"]:
                self.queue_installing_budgie()
            if self.selected_options["desktops"]["deepin"]["enabled"]:
                self.queue_installing_deepin()    
            if self.selected_options["desktops"]["xfce"]["enabled"]:
                self.queue_installing_xfce()
            if self.selected_options["desktops"]["mate"]["enabled"]:
                self.queue_installing_mate()    
            if self.selected_options["desktops"]["lxde"]["enabled"]:
                self.queue_installing_lxde()
            if self.selected_options["desktops"]["lxqt"]["enabled"]:
                self.queue_installing_lxqt()
            if self.selected_options["desktops"]["i3"]["enabled"]:
                self.queue_installing_i3()
            if self.selected_options["desktops"]["openbox"]["enabled"]:
                self.queue_installing_openbox()
            if self.selected_options["desktops"]["enlightenment"]["enabled"]:
                self.queue_installing_enlightenment()
            if self.selected_options["desktops"]["pantheon"]["enabled"]:
                self.queue_installing_pantheon()
            self += Command(["arch-chroot", "/install", "systemctl", "set-default", "graphical.target"])

    def queue_installing_display_managers(self):
        if self.installation_mode not in [ArchLinuxInstaller.MINIMAL, ArchLinuxInstaller.SILENT_MINIMAL]:
            if self.selected_options["display_managers"]["lightdm"]["enabled"]:
                self.queue_installing_lightdm()
            if self.selected_options["display_managers"]["gdm"]["enabled"]:
                self.queue_installing_gdm()
            if self.selected_options["display_managers"]["sddm"]["enabled"]:
                self.queue_installing_sddm()
            self += Command(["arch-chroot", "/install", "systemctl", "set-default", "graphical.target"])

    def queue_setting_root_credentials(self):
        self += LogMessage.Info("Setting the root password...")
        self += Command(["echo", "root:root"], is_silent= True) | Command(["arch-chroot", "/install", "chpasswd"], is_silent= True)

    def queue_user_accounts(self):
        # TODO
        pass

    def queue_installing_other_packages(self):
        other_packages: List[str] = self.selected_options["other_packages"]
        other_packages.extend(self.mode_specific_options["other_packages"])
        if other_packages:
            self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S"] + other_packages) 

    def queue_enabling_other_services(self):
        other_services: List[str] = self.selected_options["other_services"]
        other_services.extend(self.mode_specific_options["other_services"])
        if other_services:
            self += Command(["arch-chroot", "/install", "systemctl", "enable"] + other_services) 

    def queue_unmounting_partitions(self):
        self += LogMessage.Info("Unmounting partitions...") 
        self += Command(["umount", "-R", "/install"])

    def queue_installation(self):
        self.queue_partitioning()
        self.queue_package_caching()    
        self.queue_mirror_setup()
        self.queue_base_installation()
        self.queue_kernel_installation()
        self.queue_firmware_installation()
        self.queue_genfstab()
        self.queue_setting_timezone()
        self.queue_setting_locale()
        self.queue_setting_hostname()
        self.queue_setting_network_connectivity()
        self.queue_setting_root_credentials()
        self.queue_user_accounts()
        self.queue_setting_bootloader()
        self.queue_installing_desktops()
        self.queue_installing_display_managers()
        self.queue_installing_other_packages()
        self.queue_enabling_other_services()
        self.queue_unmounting_partitions()          

    def queue_installing_gnome(self):
        self += LogMessage.Info("Installing Gnome...") 
        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S", "gnome"])

        xinitrc_append_contents = \
        """
        export XDG_CURRENT_DESKTOP=GNOME-Classic:GNOME
        export GNOME_SHELL_SESSION_MODE=classic
        exec gnome-session
        """
        xinitrc_append_contents = textwrap.dedent(xinitrc_append_contents)
        self += Command(["echo", xinitrc_append_contents]) >> Path("/install/etc/X11/xinit/xinitrc")

    def queue_installing_plasma(self):
        self += LogMessage.Info("Installing KDE Plasma...") 
        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S", "plasma", "plasma-wayland-session", "egl-wayland", "kwin"])

        xinitrc_append_contents = \
        """
        export DESKTOP_SESSION=plasma
        exec startplasma-x11
        """
        xinitrc_append_contents = textwrap.dedent(xinitrc_append_contents)
        self += Command(["echo", xinitrc_append_contents]) >> Path("/install/etc/X11/xinit/xinitrc")

    def queue_installing_cinnamon(self):
        self += LogMessage.Info("Installing Cinnamon...") 
        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S", "cinnamon"])

        xinitrc_append_contents = \
        """
        exec cinnamon-session
        """
        xinitrc_append_contents = textwrap.dedent(xinitrc_append_contents)
        self += Command(["echo", xinitrc_append_contents]) >> Path("/install/etc/X11/xinit/xinitrc")

    def queue_installing_budgie(self):
        self += LogMessage.Info("Installing Budgie...") 
        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S", "budgie-desktop"])

        xinitrc_append_contents = \
        """
        export XDG_CURRENT_DESKTOP=Budgie:GNOME
        exec budgie-desktop
        """
        xinitrc_append_contents = textwrap.dedent(xinitrc_append_contents)
        self += Command(["echo", xinitrc_append_contents]) >> Path("/install/etc/X11/xinit/xinitrc")

    def queue_installing_deepin(self):
        self += LogMessage.Info("Installing Deepin...") 
        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S", "deepin"])

        xinitrc_append_contents = \
        """
        exec startdde
        """
        xinitrc_append_contents = textwrap.dedent(xinitrc_append_contents)
        self += Command(["echo", xinitrc_append_contents]) >> Path("/install/etc/X11/xinit/xinitrc")

    def queue_installing_xfce(self):
        self += LogMessage.Info("Installing XFCE...") 
        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S", "xfce4"])

        xinitrc_append_contents = \
        """
        exec startxfce4
        """
        xinitrc_append_contents = textwrap.dedent(xinitrc_append_contents)
        self += Command(["echo", xinitrc_append_contents]) >> Path("/install/etc/X11/xinit/xinitrc")

    def queue_installing_mate(self):
        self += LogMessage.Info("Installing MATE...") 
        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S", "mate"])

        xinitrc_append_contents = \
        """
        exec mate-session
        """
        xinitrc_append_contents = textwrap.dedent(xinitrc_append_contents)
        self += Command(["echo", xinitrc_append_contents]) >> Path("/install/etc/X11/xinit/xinitrc")

    def queue_installing_lxde(self):
        self += LogMessage.Info("Installing LXDE...") 
        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S", "lxde"])

        xinitrc_append_contents = \
        """
        exec startlxde
        """
        xinitrc_append_contents = textwrap.dedent(xinitrc_append_contents)
        self += Command(["echo", xinitrc_append_contents]) >> Path("/install/etc/X11/xinit/xinitrc")

    def queue_installing_lxqt(self):
        self += LogMessage.Info("Installing LXQt...") 
        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S", "lxqt"])

        xinitrc_append_contents = \
        """
        exec startlxqt
        """
        xinitrc_append_contents = textwrap.dedent(xinitrc_append_contents)
        self += Command(["echo", xinitrc_append_contents]) >> Path("/install/etc/X11/xinit/xinitrc")

    def queue_installing_i3(self):
        self += LogMessage.Info("Installing i3...") 
        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S", "i3"])

        xinitrc_append_contents = \
        """
        exec i3
        """
        xinitrc_append_contents = textwrap.dedent(xinitrc_append_contents)
        self += Command(["echo", xinitrc_append_contents]) >> Path("/install/etc/X11/xinit/xinitrc")

    def queue_installing_openbox(self):
        self += LogMessage.Info("Installing Openbox...") 
        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S", "openbox"])

        xinitrc_append_contents = \
        """
        exec openbox-session
        """
        xinitrc_append_contents = textwrap.dedent(xinitrc_append_contents)
        self += Command(["echo", xinitrc_append_contents]) >> Path("/install/etc/X11/xinit/xinitrc")

    def queue_installing_enlightenment(self):
        self += LogMessage.Info("Installing Enlightenment...") 
        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S", "enlightenment"])

        xinitrc_append_contents = \
        """
        startx /usr/bin/enlightenment_start
        """
        xinitrc_append_contents = textwrap.dedent(xinitrc_append_contents)
        self += Command(["echo", xinitrc_append_contents]) >> Path("/install/etc/X11/xinit/xinitrc")

    def queue_installing_pantheon(self):
        self += LogMessage.Info("Installing Pantheon...") 
        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S", "pantheon"])

        xinitrc_append_contents = \
        """
        wingpanel &
        plank &
        exec gala
        """
        xinitrc_append_contents = textwrap.dedent(xinitrc_append_contents)
        self += Command(["echo", xinitrc_append_contents]) >> Path("/install/etc/X11/xinit/xinitrc")

    def queue_installing_lightdm(self):
        self += LogMessage.Info("Installing LightDM...") 
        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S", "lightdm"])

        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S", "lightdm-gtk-greeter"])

        self += Command(["arch-chroot", "/install", "systemctl", "enable", "lightdm"])

        # self += LogMessage.Info("Editing lightdm configuration files...")        
        # lightdm_conf_append_contents = \
        # """        
        # [LightDM]
        # run-directory=/run/lightdm
        # [Seat:*]
        # session-wrapper=/etc/lightdm/Xsession
        # autologin-guest=false
        # # autologin-user=root
        # # autologin-user-timeout=0
        # greeter-session = lightdm-gtk-greeter
        # greeter-show-manual-login=true
        # greeter-hide-users=false
        # allow-guest=false
        # """
        # lightdm_conf_append_contents = textwrap.dedent(lightdm_conf_append_contents)
        # self += Command(["echo", lightdm_conf_append_contents]) > Path("/install/etc/lightdm/lightdm.conf")

    def queue_installing_gdm(self):
        self += LogMessage.Info("Installing GDM...") 
        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S", "gdm"])

        self += Command(["arch-chroot", "/install", "systemctl", "enable", "gdm"])

    def queue_installing_sddm(self):
        self += LogMessage.Info("Installing SDDM...") 
        self += Command(["arch-chroot", "/install", "pacman", "--needed", "--noconfirm", "-S", "sddm"])

        self += Command(["arch-chroot", "/install", "systemctl", "enable", "sddm"])

    # OVERLOADED OPERATORS

    def __iadd__(
        self,
        other: Union[Command, Function, LogMessage, BatchJob, SystemAnalyzer, AbstractInstaller]
    ) -> ArchLinuxInstaller:
        """
        Overloads the += operator to add tasks or the start methods of other batch jobs to the queue

        Parameters
        ----------
        other : Union[Command, Function, LogMessage, BatchJob]
            The second operand

        Returns
        -------
        BatchJob
            The modified BatchJob instance with the new task in queue
        """

        _ = super().__iadd__(other)
        return self
        
class RebornOSInstaller(ArchLinuxInstaller):

    """Handy tools for installing RebornOS"""

    # CONSTRUCTOR

    def __init__(
        self,
        thread_name:str= "RebornOSInstallJob",
        logging_handler: Optional[LoggingHandler] = None,
        pre_run_function: functools.partial = None,
        post_run_function: functools.partial = None
    ) -> None:
        """
        Initialize an 'RebornOSInstaller' object
        
        Parameters
        ----------
        thread_name: str
            Name for prefixing the threads
        logging_handler: Optional[LoggingHandler]
            The LoggingHandler object which stores the logging functions, logging threads, logger information, etc.
        pre_run_function: functools.partial
            A function to be called before the monitored job starts. Its name and arguments are wrapped together by calling functools.partial
        post_run_function: functools.partial
            A function to be called after the monitored job finishes. Its name and arguments are wrapped together by calling functools.partial
        """

        # Call the super class constructor from ArchLinuxInstaller
        super().__init__(
            thread_name= thread_name,
            logging_handler= logging_handler,
            pre_run_function= pre_run_function,
            post_run_function= post_run_function
        )
   
    # OVERLOADED OPERATORS

    def __iadd__(
        self,
        other: Union[Command, Function, LogMessage, BatchJob]
    ) -> RebornOSInstaller:
        """
        Overloads the += operator to add tasks or the start methods of other batch jobs to the queue

        Parameters
        ----------
        other : Union[Command, Function, LogMessage, BatchJob]
            The second operand

        Returns
        -------
        BatchJob
            The modified BatchJob instance with the new task in queue
        """

        _ = super().__iadd__(other)
        return self
