user\_interface.gtk.code
========================

.. automodule:: user_interface.gtk.code
  
   
   
   

   
   
   

   
   
   

   
   
   



.. rubric:: Modules

.. autosummary::
   :toctree:
   :template: custom-module-template.rst                
   :recursive:

   user_interface.gtk.code.appearance_page
   user_interface.gtk.code.appgroups_page
   user_interface.gtk.code.desktops_page
   user_interface.gtk.code.display_managers_page
   user_interface.gtk.code.finish_page
   user_interface.gtk.code.games_page
   user_interface.gtk.code.identification_page
   user_interface.gtk.code.installing_page
   user_interface.gtk.code.introduction_page
   user_interface.gtk.code.locale_page
   user_interface.gtk.code.main
   user_interface.gtk.code.mode_page
   user_interface.gtk.code.partition_page
   user_interface.gtk.code.productivity_page
   user_interface.gtk.code.security_page
   user_interface.gtk.code.users_page
   user_interface.gtk.code.utilities_page
   user_interface.gtk.code.welcome_page

