library.installing.AbstractInstaller
====================================

.. currentmodule:: library.installing

.. inheritance-diagram::
      AbstractInstaller

|

.. autoclass:: AbstractInstaller
   :members:    
   :undoc-members:
   :private-members:
   :special-members:                                
   :show-inheritance:                          
  
   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~AbstractInstaller.__init__
      ~AbstractInstaller.abort
      ~AbstractInstaller.call_post_run_function
      ~AbstractInstaller.call_pre_run_function
      ~AbstractInstaller.clear_tasks
      ~AbstractInstaller.process_system_analysis
      ~AbstractInstaller.queue
      ~AbstractInstaller.start
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~AbstractInstaller.BASIC
      ~AbstractInstaller.CUSTOMIZED
      ~AbstractInstaller.FULL
      ~AbstractInstaller.MINIMAL
      ~AbstractInstaller.SEMI_AUTOMATIC
      ~AbstractInstaller.SILENT_BASIC
      ~AbstractInstaller.SILENT_FULL
      ~AbstractInstaller.SILENT_MINIMAL
   
   