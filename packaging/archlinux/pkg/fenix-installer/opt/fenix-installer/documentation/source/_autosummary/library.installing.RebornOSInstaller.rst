library.installing.RebornOSInstaller
====================================

.. currentmodule:: library.installing

.. inheritance-diagram::
      RebornOSInstaller

|

.. autoclass:: RebornOSInstaller
   :members:    
   :undoc-members:
   :private-members:
   :special-members:                                
   :show-inheritance:                          
  
   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~RebornOSInstaller.__init__
      ~RebornOSInstaller.abort
      ~RebornOSInstaller.call_post_run_function
      ~RebornOSInstaller.call_pre_run_function
      ~RebornOSInstaller.clear_tasks
      ~RebornOSInstaller.process_system_analysis
      ~RebornOSInstaller.queue
      ~RebornOSInstaller.queue_base_installation
      ~RebornOSInstaller.queue_enabling_other_services
      ~RebornOSInstaller.queue_firmware_installation
      ~RebornOSInstaller.queue_genfstab
      ~RebornOSInstaller.queue_installation
      ~RebornOSInstaller.queue_installing_budgie
      ~RebornOSInstaller.queue_installing_cinnamon
      ~RebornOSInstaller.queue_installing_deepin
      ~RebornOSInstaller.queue_installing_desktops
      ~RebornOSInstaller.queue_installing_display_managers
      ~RebornOSInstaller.queue_installing_enlightenment
      ~RebornOSInstaller.queue_installing_gdm
      ~RebornOSInstaller.queue_installing_gnome
      ~RebornOSInstaller.queue_installing_i3
      ~RebornOSInstaller.queue_installing_lightdm
      ~RebornOSInstaller.queue_installing_lxde
      ~RebornOSInstaller.queue_installing_lxqt
      ~RebornOSInstaller.queue_installing_mate
      ~RebornOSInstaller.queue_installing_openbox
      ~RebornOSInstaller.queue_installing_other_packages
      ~RebornOSInstaller.queue_installing_pantheon
      ~RebornOSInstaller.queue_installing_plasma
      ~RebornOSInstaller.queue_installing_sddm
      ~RebornOSInstaller.queue_installing_xfce
      ~RebornOSInstaller.queue_kernel_installation
      ~RebornOSInstaller.queue_mirror_setup
      ~RebornOSInstaller.queue_package_caching
      ~RebornOSInstaller.queue_partitioning
      ~RebornOSInstaller.queue_setting_bootloader
      ~RebornOSInstaller.queue_setting_hostname
      ~RebornOSInstaller.queue_setting_locale
      ~RebornOSInstaller.queue_setting_network_connectivity
      ~RebornOSInstaller.queue_setting_root_credentials
      ~RebornOSInstaller.queue_setting_timezone
      ~RebornOSInstaller.queue_unmounting_partitions
      ~RebornOSInstaller.queue_user_accounts
      ~RebornOSInstaller.start
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~RebornOSInstaller.BASIC
      ~RebornOSInstaller.CUSTOMIZED
      ~RebornOSInstaller.FULL
      ~RebornOSInstaller.MINIMAL
      ~RebornOSInstaller.SEMI_AUTOMATIC
      ~RebornOSInstaller.SILENT_BASIC
      ~RebornOSInstaller.SILENT_FULL
      ~RebornOSInstaller.SILENT_MINIMAL
   
   