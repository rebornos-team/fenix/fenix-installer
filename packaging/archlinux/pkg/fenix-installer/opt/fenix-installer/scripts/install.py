# Fenix Installer
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)
# 2. 

# IMPORTS

import functools
from typing import Optional, Union

# FENIX IMPORTS

from fenix_library.configuration import JSONConfiguration
from fenix_library.running import LogMessage, Function, Command, BatchJob, LoggingHandler
from library.analyzing import SystemAnalyzer
from library.installing import RebornOSInstaller, ArchLinuxInstaller

# REGULAR FUNCTIONS

def install(
    logging_handler: Optional[LoggingHandler] = None,
    pre_install_function: Optional[functools.partial] = None,
    post_install_function: Optional[functools.partial] = None
) -> Optional[Union[BatchJob, ArchLinuxInstaller]]:
    """
    A helper function to centrally control what to do when the user initiates an install on any user interface.
    The following tasks are performed
    - If "on_a_virtual_machine" is false in configuration/installer.json, perform harmless tests
    - If "on_a_virtual_machine" is true in configuration/installer.json, perform any installation tasks

    Parameters
    ----------
    pre_install_function: Optional[functools.partial], default None
        The function to call before the installation process. Its name and arguments are wrapped together by calling functools.partial
    post_install_function: Optional[functools.partial], default None
        The function to call after the installation process. Its name and arguments are wrapped together by  functools.partial
    logging_handler: Optional[LoggingHandler]
        The LoggingHandler object which stores the logging functions, logging threads, logger information, etc.

    Returns
    -------
    Nothing
    """

    installer_settings = JSONConfiguration("configuration/installer.json")    

    if not installer_settings["on_a_virtual_machine"]: # Run harmless commmands to test 

        # Create a batch job to dispatch multiple tasks in a sequence on a separate thread
        harmless_job = BatchJob(
            thread_name= "HarmlessJob",
            logging_handler= logging_handler,
            pre_run_function= pre_install_function,
            post_run_function= post_install_function
        )
        # Set up a system analyzer
        system_analyzer = SystemAnalyzer(
            thread_name= "SystemStatusChecks",
            logging_handler= logging_handler
        )
        harmless_job += system_analyzer
        harmless_job += LogMessage.Info("Harmless test commands will be run now (not an actual installation)...")
        harmless_job += LogMessage.Warning("If you modify \"on_a_virtual_machine\" on `configuration/installer.json`, please don't upload or sync any changes to Gitlab. Also, do not copy it back onto your local hard drive. This setting enables running dangerous commands.")
        harmless_job += LogMessage.Info("Starting a ping to Google, to test the post-run function feature. Here, we enable sending the output of the command/script as the first argument to the post-run function, that is called immedietly after the ping. The reason this feature exists is because sometimes you need the output of the previous command to run another task after completion, like when you run a command to check internet connection and want to inform the user that there is no connectivity.")
        harmless_job += Command(
            ["ping", "-c", "5", "www.google.com"],
            pre_run_function= functools.partial(print, "The following ping output must also appear on the `stdout` console because of the post-run function, to which we enable sending output."),
            post_run_function= functools.partial(print, "Second test argument to the post-run print function...", "\nThird test argument to the post-run print function.."),
            do_send_output_to_post_run_function= True
        )
        harmless_job += LogMessage.Info("Done pinging google...")
        harmless_job += LogMessage.Info("Now testing if we can queue a print function in this job (outputs to the console at stdout), while passing multiple arguments to it...")
        harmless_job += Function(
            print,
            "First test argument to the queued print function...",
            "\nSecond test argument to the queued print function..."
        )
        harmless_job += LogMessage.Info("Starting a script that has an error...")
        harmless_job += Command.Script("scripts/test/harmless_script.sh")
        harmless_job += LogMessage.Info("Done executing the script...")
        harmless_job += LogMessage.Info("Pinging Amazon...")
        harmless_job += LogMessage.Info("We will test if this command can be run silently...")
        harmless_job += Command.Shell("ping -c 5 www.amazon.com", is_silent= True)
        harmless_job += LogMessage.Info("Done pinging Amazon... Was it silent? Did you see the ping, as you did for Google?")
        func = Function(print, "\nInstallation finished...")
        func.is_silent= True # Notice that this only suppresses logging. The print function can still do its job
        harmless_job += func
        harmless_job += LogMessage.Info("\nInstallation finished...") 

        harmless_job.start()    
        return harmless_job   

    elif installer_settings["on_a_virtual_machine"]: # Run an actual installation (potentially harmful)
        
        installer = RebornOSInstaller(
            thread_name= "InstallerJob",
            logging_handler= logging_handler,
            pre_run_function= pre_install_function,
            post_run_function= post_install_function
        )
        
        installer.start()
        return installer