
# Fenix Installer - RebornOS

*Documentation by @shivanandvp (shivanandvp@rebornos.org, shivanand.pattanshetti@gmail.com)*  

*Artwork by @Trivoxel*

*Please refer to [`LICENSE`](./LICENSE) for license information.*

<!-- *For a detailed API documentation, please check [this page](https://rebornos-team.gitlab.io/fenix-installer/annotated.html)*

*If you would like to help develop Fenix installer, please check [this page](./CONTRIBUTING.md)* -->

## Overview

Fenix installer consists of Gtk front-end and a UI-agnostic core that performs tasks that constitute an operating system installation. It is written in `Python`.

Some features:

* *Live* logging accomplished using reusable thread pools.
* Easy configuration by editing `JSON` files.
* Human-readable code that strives to use names that are clear and unambiguous.
* `Gtk` front-end.

## [PLEASE CLICK HERE](https://rebornos-team.gitlab.io/fenix/fenix-installer/index.html) for the full documentation
  
![](media/screenshots/1.png)

![](media/screenshots/2.png)

![](media/screenshots/3.png)

![](media/screenshots/4.png)

![](media/screenshots/5.png)

![](media/screenshots/console_output.gif)

![](media/screenshots/7.png)
