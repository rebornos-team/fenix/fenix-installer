#! /usr/bin/env sh

# Fenix Installer
# Please refer to the file `LICENSE` in the current directory for license information. 
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)
# 2. 

# WARNING: 
# This script enables commands that should only be run on a VM (during Fenix's development)
# Do not run this script on anything other than a VM. It will wipe the storage

