#! /usr/bin/env sh

# Fenix Installer
# Please refer to the file `LICENSE` in the current directory for license information. 
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)
# 2. Keegan Milsten

#Check whether or not the system is EUFI
if [ -d /sys/firmware/efi/efivars ]; then
    bootmode="eufi"
    mount=mnt
    boot=efi
else
    bootmode="bios"
    mount=mnt
fi

# Output bootmode
echo "Intalling RebornOS to $bootmode hardware..."

# Declare variable for system identity (used for the Grub ID, hostname file, etc
identity=RebornOS

# Rank mirrors
reflector --verbose -p https --sort rate --save /etc/pacman.d/mirrorlist

# Create partitions and write the changes
(
echo o
echo Y
echo n
echo 1
echo 2048
echo +512M
echo 8300
echo n
echo 2
echo 
echo +1M
echo EF02
echo n
echo 3
echo 
echo 
echo 8300
echo w
echo Y
) | gdisk /dev/sda

# Format the partition and create an ext4 file system
mkfs.ext4 /dev/sda1
mkfs.ext4 /dev/sda3

# Mount the partitions
mount /dev/sda3 /mnt
cd /mnt
mkdir boot
mount /dev/sda1 boot

# Install the base package
pacstrap /mnt base grub

# Generate an fstab file
genfstab -L /mnt >> /mnt/etc/fstab

# Change into the installation root
cat << EOF | arch-chroot /$mount
    # Install Packages
    pacstrap /mnt base
    # Set timezone and locale
    ln -sf /usr/share/zoneinfo/America/Chicago /etc/localtime
    hwclock --systohc --utc
    locale-gen
    echo LANG=en_US.UTF-8 > /etc/locale.conf
    export LANG=en_US.UTF-8

    # Set hostname
    echo $identity > /etc/hostname
    
    # Set Password
    #passwd blah blah blah

    systemctl enable dhcpcd
EOF
cat << EOF | arch-chroot /mnt
    # Install bootloader
    pacman -S grub os-prober
    Y
EOF
cat << EOF | arch-chroot /$mount
if [ $bootmode == "bios" ]; then
    grub-install /dev/sda
    grub-mkconfig -o /boot/grub/grub.cfg
else
    grub-install --target=x86_64-efi --efi-directory=/$mount/$boot --boot-directory=/$mount/$boot --bootloader-id=$identity
    grub-mkconfig -o /boot/grub/grub.cfg
fi
EOF
