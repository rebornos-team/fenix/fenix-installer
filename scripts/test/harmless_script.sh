#! /usr/bin/env sh

# Fenix Installer
# Please refer to the file `LICENSE` in the current directory for license information. 
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)
# 2. Keegan Milsten (rebornos@protonmail.com)


#Check whether or not the system is EUFI
if [ -d /sys/firmware/efi/efivars ]; then
    bootmode="eufi"
else
    bootmode="bios"
fi

# Output bootmode
echo "A test echo inside a script"

date 'Deliberately wrong date...'

ping -c 5 www.facebook.com

if [ "abc" = "ghi" ]; then
    echo expression evaluated as true
else
    echo expression evaluated as false
fi
