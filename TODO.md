# ToDo

- Start over option - To reset choices of desktops and other packages rather than keeping them "sticky"
- Offline installation - Already works. Just need a button, checkbox, or something
- Specify external cache location - Already works. Just need a text field
- Choosing time
- Choosing locales
- Choosing display manager (lightdm, gdm, etc.)
- Setting locker
- Add grub entries for kernels selected
- Alternative bootladers
- Partitioning
- Alternative file systems
- Encryption
- User account creation
- System analysis screen
- Pre-Installation summary screen
- Choosing boot screen theme
- Choosing greeters