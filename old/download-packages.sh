#!/bin/bash

# Fenix Installer
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)
# 2. 

# Example use
# ./download-packages.sh -i "package_input.list" -o "/run/media/shivanandvp/DATA/Development/Live Media/VMs/ArchLinuxInstallationTesting/Shared/package_cache"

print_usage() {                                 # Function: Print a help message.
  echo "Usage: $0 [ -i package_list_filepath ] [ -o output_directory_path ]" 1>&2 
}

wrong_usage() {  
  print_usage
  exit 1
}

# Update databases
sudo pacman -Syy

# Describe options for input file and output directory
while getopts ":i:o:" opt; do
     case "$opt" in
         i ) inputFile="$OPTARG";;
         o ) outputDirectory="$OPTARG";;
         : ) echo "Error: -${OPTARG} requires an argument."
             wrong_usage;;
         * ) wrong_usage;;
     esac
done

ARCH="x86_64"
MIRROR="https://mirrors.kernel.org/archlinux/"
wget -P "$outputDirectory" "${MIRROR}/community/os/${ARCH}/community.db"
wget -P "$outputDirectory" "${MIRROR}/core/os/${ARCH}/core.db"
wget -P "$outputDirectory" "${MIRROR}/extra/os/${ARCH}/extra.db"
wget -P "$outputDirectory" "${MIRROR}/multilib/os/${ARCH}/multilib.db"

IFS=$'\n' read -d '' -r -a packages < "$inputFile"

rm -f "$outputDirectory/fenix_packages_to_install.list"
for package in "${packages[@]}"; do
   pactree --ascii --linear --sync --unique $package >> "$outputDirectory/fenix_packages_to_install.list"
done

sort --ignore-case --unique "$outputDirectory/fenix_packages_to_install.list" > "$outputDirectory/fenix_packages_to_install.list.temp"
pacman -Sp $(cat "$outputDirectory/fenix_packages_to_install.list.temp") > "$outputDirectory/fenix_packages_to_install.list"
rm -f "$outputDirectory/fenix_packages_to_install.list.temp"

(cd "$outputDirectory" && xargs -n 1 curl -O < "fenix_packages_to_install.list")