file_str = """
%PACKAGER%
Unknown Packager

%DEPENDS%
gdk-pixbuf2

%OPTDEPENDS%
qt5-base: For the Qt backend
cairo: For the cairo backend
kio: For the dolphin thumbnailer

%MAKEDEPENDS%
cargo
clang
qt5-base
qt5-tools
kio
cairo
pango
cmake
extra-cmake-modules
"""

records = file_str.split("\n\n")
json_dict = {}
for record in records:
    entries = record.strip().split("%")
    key = entries[1].strip()
    values = entries[2].strip()
    values = values.split("\n")
    json_dict[key] = values

import pprint
pprint(json_dict)