#!/bin/bash

# Fenix Installer
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-installer

# AUTHORS
# 1. Keegan Milsten
# 2. 

# Bash function to locate key values from options.json

function user_input() {

    # `cd` to script location while running this function
    local DIR="${0%/*}"
    cd $DIR

    # Copy the json file we are using to a temporary one we will prune, called `options.tmp`
    local file=../../configuration/options.json # Rename the json file to whatever is necessary
    cp -f $file ./options.tmp

    # Number of parameters passed to function
    local number_of_parameters=$#

    # Declare variable that holds the value of the current parameter that we are on in any iteration of the loop below
    local current_parameter_number=1

    # Declare variable that serves to record the line number of the key being changed
    local line_number=0

    # Declare variable that records the amount of whitespace preceding the key being changed
    local whitespace=""

    # Loop over all the parameters
    for current_parameter in $@; do

        # Increment `current_parameter_number` by 1 in each iteration
        local current_parameter_number=$(($current_parameter_number+1))

        # IF it is not the last parameter passed, run the following
        if [[ $current_parameter_number -lt $number_of_parameters ]] ; then

            # Increase the amount of whitespace by 4 (in other words, an indent)
            for ((i = 0 ; i < 4 ; i++)); do
                local whitespace+=" "
            done

            #sed "/^$current_parameter$/,/^}$/{ /^$current_parameter$/d; /^}$whitespace$/d; p; }" ./options.tmp

            # Output all text nested under the current parameter to ./options.tmp
            starting_line=$(grep -n "\"$current_parameter\":" ./options.tmp | awk '{print $1}' | tr -d :)
            sed -i "1,$starting_line d" ./options.tmp

            local closing_bracket=1
            local last_line=0
            while read line; do
                local last_line=$(( $last_line + 1 ))
                local bracket=$(echo -e $line | grep -E '{|}|},' | wc -l)

                if [[ $bracket -gt 0 ]]; then
                    local closing_bracket=$(( closing_bracket + 1 ))
                    local final_bracket=$(echo -e $line | grep '}' | wc -l)
                    if [[ $(( closing_bracket % 2 )) -eq 1 ]] && [[ $final_bracket -gt 0 ]]; then
                         {
                             head -$last_line ./options.tmp | tee ./options_temp.tmp
                         } #>/dev/null 2>&1
                         cp ./options_temp.tmp ./options.tmp
                         break
                    fi
                    # echo
                    # echo "bracket: $bracket"
                    # echo "last_line: $last_line"
                    # echo "closing_bracket: $closing_bracket"
                    # echo "final_bracket: $final_bracket"
                    # echo
                fi
            done < ./options.tmp

            # Add the line number of the current parameter to the previous value of `line_number`
            local line_number=$(( $(grep -n $current_parameter ./options.tmp | awk '{print $1}' | tr -d :) + $line_number ))

            echo $line_number
        elif [[ $current_parameter_number -eq $number_of_parameters ]] ; then
            local value="\"$current_parameter\""
            local value+=": "
            echo $value
        #
        # else
        #     # Variable to hold the key and its value
        #     local value=$(echo -e $line_content | cut -f1 -d ":")
        #
        #     # If the current parameter is a number, do NOT add any quotation marks around it.
        #     # Otherwise, do add quotation marks around it.
        #     if [[ $current_parameter =~ ^[0-9]+([.][0-9]+)?$ ]] ; then
        #        local value+=": $current_parameter"
        #     else
        #        local value+=": \"$current_parameter\""
        #     fi
        #
        #     # If `value` is not the last nested key in the section, then append a `,` to it
        #     if [[ $last_nested_value -gt 0 ]] ; then
        #         local value+=","
        #     fi
        #
        #     # Write the `value` with the proper amount of whitespace before it
        #     # to `$file`
        #     sed "$line_number s|.*|$whitespace$value|" $file > ./options.txt
        #     mv ./options.txt $file
        fi
    done

    # Remove ./options.tmp as we are done using it
    # rm -f ./options.tmp
}

export -f user_input
"$@"

# Exporting this function to be used elsewhere.
# To use it, simply implement the following format:
#
#   path/to/assign-key.sh user_input level1 level2 level3 new_value
#
# For instance, if you have a file like this:

#    "time_page": {
#       "timezone": {
#           "zone": "Los_Angeles"
#       }
#    }

# You would call the function this way then if it is a city, as bash does not support the creation of classes:
#
#   path/to/assign-key.sh user_input time_page timezone zone Los_Angeles
#
# If the key value is numerical, you would call it the same way, like this:
#
#   path/to/assign-key.sh time_page timezone zone 3
#
# - In Bash, the function parameters are listed AFTER the function,
# rather than inside it like most languages. Hence, the `user_input value`
# formatting above ;).

## NOTE
# - In addition, bash does not support returning strings. To get around that, we must bind a variable
# to the function itself, in this case the `result` variable.

# - Also, this script is designed to add quotation marks around non-numerical key values you input,
# so no need to worry about syntax when inputting a string or number

## IDEA: Create variable at the top of your script, like this:
#   input_function=$(path/to/collect-input.sh user_input)
#
# and then just call that variable in conjunction with the necessary parameters when necessary so as to
# make your code cleaner. That way, whenever you need to utilise this script later on in your file,
# you can do so like this:
#   result=$($input_function time_page timezone zone)
