#!/bin/bash

# Fenix Installer
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-installer

# AUTHORS
# 1. Keegan Milsten
# 2. 

# Bash function to locate key values from options.json

function user_input() {

    # `cd` to script location while running this function
    local DIR="${0%/*}"
    cd $DIR

    # Copy the json file we are using to a temporary one we will prune, called `options.tmp`
    local file=../../configuration/options.json # Rename the json file to whatever is necessary
    cp $file ./options.tmp

    # Number of parameters passed to function
    local number_of_parameters=$#

    # Declare variable that holds the value of the current parameter that we are on in any iteration of the loop below
    local current_parameter_number=1

    # Loop over all the parameters
    for current_parameter in $@; do

        # Increment `current_parameter_number` by 1 in each iteration
        local current_parameter_number=$(($current_parameter_number+1))

        # IF it is not the last parameter passed, run the following
        if [[ $current_parameter_number -le $number_of_parameters ]] ; then
            # Output all text nested under the current parameter to ./options.tmp
            {
                awk "/$current_parameter/{flag=1;next}/}/{flag=0}flag" ./options.tmp | tee ./options_temp.tmp
            } >/dev/null 2>&1
            mv ./options_temp.tmp ./options.tmp
        else
            # IF it is the last parameter passed, find the matching line in ./options.tmp
            # and save its key value to the variable `value`
            line_content=$(grep "$current_parameter" ./options.tmp)
            local value=${line_content#*:}
            local value=$(echo -e "${value}" | tr -d '[:space:]')
            local value=$(echo -e $value | tr -d ,)
            local value=$(echo -e $value | tr -d \")
        fi
    done

    # Remove ./options.tmp as we are done using it
    rm -f ./options.tmp

    # Return the key value that $value holds
    echo -e $value
}

export -f user_input
"$@"

# Exporting this function to be used elsewhere.
# To use it, simply implement the following format:
#
#   path/to/collect-input.sh user_input level1 level2 level3
#
# For instance, if you have a file like this:

#    "time_page": {
#       "timezone": {
#           "zone": "Los_Angeles"
#       }
#    }

# You would call the function this way then, as bash does not support the creation of classes:
#
#   result=$(path/to/collect-input.sh user_input time_page timezone zone)
#
# - In Bash, the function parameters are listed AFTER the function,
# rather than inside it like most languages. Hence, the `user_input value`
# formatting above ;).

## NOTE
# - In addition, bash does not support returning strings. To get around that, we must bind a variable
# to the function itself, in this case the `result` variable.

## IDEA: Create variable at the top of your script, like this:
#   input_function=$(path/to/collect-input.sh user_input)
#
# and then just call that variable in conjunction with the necessary parameters when necessary so as to
# make your code cleaner. That way, whenever you need to utilise this script later on in your file,
# you can do so like this:
#   result=$($input_function time_page timezone zone)
