# Fenix Installer
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# GUI
# 1. Trivoxel (https://gitlab.com/TriVoxel)

# FENIX IMPORTS
from fenix_library.configuration import JSONConfiguration
from user_interface.gtk.utilities.page import FenixInstallerPage, PageTools
from user_interface.gtk.utilities.image import ImageTools
from user_interface.gtk.utilities.text import TextTools

# CUSTOM IMPORTS
import gi # Python GObject introspection module which contains Python bindings and support for Gtk
gi.require_version('Gtk', '3.0') # make sure that the Gtk version is at the required level
from gi.repository import Gtk # Gtk related modules for the graphical interface
from gi.repository import GdkPixbuf # Gtk module that describes images
from gi.repository import GLib # for using a function that will allow updating the on-screen console text

# Unique name for use in configuration files for looking up details about this page
# ----------- Modify this ---------- #
CURRENT_PAGE_NAME = "desktops"
# ----------- Modify this ---------- #

class Page(FenixInstallerPage):
# create a page class derived from "FenixInstallerPage" which contains a page template

    # CONSTRUCTOR
    def __init__(self):
        super().__init__(CURRENT_PAGE_NAME) # call the super-class constructor that handles all the tasks associated with page creation automatically
        self.builder.connect_signals(self)                           # connect the signals from the Gtk form to our event handlers

        # ---------- Custom code ----------- #
        self.common_settings = JSONConfiguration("configuration/user_selected/common_options.json")
        desktop_stack: Gtk.Stack = self.builder.get_object("desktop_stack")
        self.slidePaths = []
        self.slideImages  = []
        self.slideNames = []
        for desktop_entry in self.common_settings["desktops"]:
            self.slideNames.append(desktop_entry)
            filepath = self.common_settings["desktops"][desktop_entry]["picture_path"]
            self.slidePaths.append(filepath)
            desktop_image_pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(
                filename= filepath,
                width= 650,
                height= -1
            )
            desktop_image = Gtk.Image.new_from_pixbuf(desktop_image_pixbuf)  
            self.slideImages.append(desktop_image)
            scrolledWindow = Gtk.ScrolledWindow.new(None, None)
            viewport = Gtk.Viewport.new(None, None)
            viewport.add(desktop_image)
            scrolledWindow.add(viewport)
            desktop_stack.add_titled(
                scrolledWindow,
                desktop_entry,
                self.common_settings["desktops"][desktop_entry]["title"]
            )
            self.common_settings["desktops"][desktop_entry]["enabled"] = False
        self.common_settings.write_data()
        desktop_stack.show_all()
        self.builder.get_object("desktop_switch").set_state(self.common_settings["desktops"][desktop_stack.get_visible_child_name()]["enabled"])
        # -------- Custom code ends -------- #

    # EVENT HANDLERS
    # To specify how this particular Gtk container handles user interactions. The names of handler functions can be assigned in `Glade` under "Signals"
    # Create functions (with names as described above) that are called when specific UI events take place
    def on_desktop_page_resize(self, stack, rectangle):
        self.resize_visible_image(stack)

    def on_desktop_stack_change(self, stack, gparamstring):
        self.builder.get_object("desktop_switch").set_state(self.common_settings["desktops"][stack.get_visible_child_name()]["enabled"])
        self.resize_visible_image(stack)

    def on_desktop_left_button_pressed(self, stack):
        index = self.slideNames.index(stack.get_visible_child_name())
        stack.set_visible_child_name(self.slideNames[index - 1])

    def on_desktop_right_button_pressed(self, stack):
        index = self.slideNames.index(stack.get_visible_child_name())
        if index < len(self.slideNames) - 1:
            stack.set_visible_child_name(self.slideNames[index + 1])
        elif index == len(self.slideNames) - 1:
            stack.set_visible_child_name(self.slideNames[0])

    def on_desktop_switch_state_set(self, stack, state):
        self.common_settings.load_data()
        self.common_settings["desktops"][stack.get_visible_child_name()]["enabled"] = state
        self.common_settings.write_data()
        return False

    # CUSTOM METHODS
    def resize_visible_image(self, stack):
        width  = stack.get_allocated_width()
        height = stack.get_allocated_height()
        desktop_page = stack.get_visible_child()
        index = stack.child_get_property(desktop_page, "position")
        image_height = self.slideImages[index].get_pixbuf().get_height()
        image_width = self.slideImages[index].get_pixbuf().get_width()
        if (float(image_width)/image_height) < float(width)/height:
            pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(
                filename= self.slidePaths[index],
                width= -1,
                height= height
            )
            self.slideImages[index].set_from_pixbuf(pixbuf)
            stack.queue_draw()
        else:
            pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(
                filename= self.slidePaths[index],
                width= width,
                height= -1
            )
            self.slideImages[index].set_from_pixbuf(pixbuf)
            stack.queue_draw()
        