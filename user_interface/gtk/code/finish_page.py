# Fenix Installer
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# GUI
# 1. Trivoxel (https://gitlab.com/TriVoxel)

# FENIX IMPORTS
from fenix_library.configuration import JSONConfiguration
from user_interface.gtk.utilities.page import FenixInstallerPage, PageTools
from user_interface.gtk.utilities.image import ImageTools
from user_interface.gtk.utilities.text import TextTools

# CUSTOM IMPORTS


# Unique name for use in configuration files for looking up details about this page
# ----------- Modify this ---------- #
CURRENT_PAGE_NAME = "finish"
# ----------- Modify this ---------- #

class Page(FenixInstallerPage):
# create a page class derived from "FenixInstallerPage" which contains a page template

    # CONSTRUCTOR
    def __init__(self):
        super().__init__(CURRENT_PAGE_NAME) # call the super-class constructor that handles all the tasks associated with page creation automatically

        # ---------- Custom code ----------- #
        self.addFinishMessage()
        # -------- Custom code ends -------- #

    # EVENT HANDLERS
    # To specify how this particular Gtk container handles user interactions. The names of handler functions can be assigned in `Glade` under "Signals"
    # Create functions (with names as described above) that are called when specific UI events take place


    # CUSTOM METHODS
    def addFinishMessage(self):
        introductionTextBuffer = self.builder.get_object("finish_message").get_buffer()
        TextTools.addBasicTextTags(introductionTextBuffer)
        iter = introductionTextBuffer.get_start_iter()
        introductionTextBuffer.insert_with_tags_by_name(iter, "Fenix Installer has finished installing RebornOS. Please restart your computer or continue using the live ISO.")
        introductionTextBuffer.insert_with_tags_by_name(iter, "\n\nThanks for trying out RebornOS!", "bold")
