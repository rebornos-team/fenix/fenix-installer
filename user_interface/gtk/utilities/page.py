# Fenix Installer
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)
# 2. 

# IMPORTS
import datetime # for time stamps
import importlib # for dynamic imports                                    
import os # for deleting files
import threading # for multithreading
import subprocess
from fenix_library.running.running import LoggingHandler # for running commands and scripts
import gi # Python GObject introspection module which contains Python bindings and support for Gtk
gi.require_version('Gtk', '3.0') # make sure that the Gtk version is at the required level
from gi.repository import Gtk # Gtk related modules for the graphical interface
from gi.repository import GdkPixbuf # Gtk module that describes images
from gi.repository import GLib # for using a function that will allow updating the on-screen console text
from gi.repository import Pango # module for rendering formatted text
from typing import List
from typing import Optional, Tuple, Union, Dict, Any
from types import ModuleType
import logging
from pathlib import Path

# FENIX IMPORTS
from fenix_library.configuration import JSONConfiguration
from fenix_library.running import BatchJob, LogMessage, LoggingHandler

logger = logging.getLogger('fenix_installer.ui.gtk.utilities'+'.'+ Path(__file__).stem)
logging_handler = LoggingHandler(logger=logger)

class FenixInstallerPage(Gtk.Box):
    """
    Create a Gtk box to host the GUI elements

    Performs all major tasks required to create an installer page on the 
    
    """

    # STATIC VARIABLES
    installer_job: Optional[BatchJob] = None
    main_builder: Gtk.Builder = None
    console_buffer: Gtk.TextBuffer = None

    # CONSTRUCTOR
    def __init__(self, current_page_name: str) -> None:
        """Constructor to run the tasks that have to be accomplished at the beginning of every installer page.
        
        Parameters
        ----------
        current_page_name: str
            Name of the current installer page

        Returns
        -------
        Nothing
        """

        self.logger = logging.getLogger('fenix_installer.ui.gtk.code'+'.'+ current_page_name)
        self.logging_handler = LoggingHandler(logger=self.logger)

        Gtk.Box.__init__(self) # call the super-class constructor        
        self._form_file_path, self._Gtk_ID = self._get_page_info(current_page_name) # read configuration files to find out information about the page
        self.builder = self._get_form_objects(self._form_file_path) # get form objects (in the form of a Gtk builder) from the form file
        self._add_form_objects(self.builder, self._Gtk_ID) # add the form objects to the current page's container
        self.builder.connect_signals(self) # connect the signals from the Gtk form to our event handlers 

    # PRIVATE METHODS

    def _add_form_objects(self, builder: Gtk.Builder, containerID: str) -> None:

        """Looks up the specified container represented by containerID within the builder and adds it to the super class (which is a Gtk.Box)

        Parameters
        ----------
        builder: Gtk.Builder
            The given Gtk.Builder in which the container represented by containerID has to be looked up.
        containerID: str
            The container ID which has to be looked up in the specified Gtk.Builder.

        Returns
        -------
        Nothing
        """

        page = builder.get_object(containerID) # get the Gtk container for this page
        self.pack_start(page, True, True, 0) # add the extracted GUI elements to the current box  

    @staticmethod
    def _get_page_info(current_page_name: str) -> Tuple[str, str]:   
        """Lookup and return details of the current page after using the current page name to search.
        
        Parameters
        ----------
        current_page_name : str
            Name of the current page
        
        Returns
        -------
        formFilePath : str
            The path to the glade file that describes how the current page is supposed to look.
        GtkID : str
            The ID of the Gtk.Box container that describes this page. This would have already been assigned in the glade file.
        """

        page_configuration = JSONConfiguration("configuration/pages.json")                                    #                                        
        formFilePath = os.path.join("user_interface", "gtk", "forms", page_configuration["page_directory"][current_page_name]["filename"] + ".glade") # get the UI file path for the current page
        GtkID = page_configuration["page_directory"][current_page_name]["Gtk_ID"] # get the Gtk ID for the UI file for the top-level container in this page

        return formFilePath, GtkID

    @staticmethod
    def _get_form_objects(formFilePath: str) -> Gtk.Builder:

        """Returns access to the UI objects of the current page given the path to the page's glade file that describes how the current page is supposed to look.
        
        Parameters
        ----------
        formFilePath: str
            The path to the glade file that describes how the current page is supposed to look.
        
        Returns
        -------
        builder : Gtk.Builder
            The Builder object associated with the current page. This could be used to access UI objects within the current page.
        """

        builder = Gtk.Builder()             # 
        builder.add_from_file(formFilePath) # create Gtk objects from the UI file

        return builder

    # REGULAR METHODS

    def append_to_console_text(
        self,
        logging_level: int = LogMessage.INFO.value,  
        message: str = "",              
        *args: Tuple[Any],
        **kwargs: Dict[str, Any]
    ) -> None:
        """Adds text to be displayed at the end of the GUI console
        
        Parameters
        ----------
        logging_level: LoggingLevel, default LoggingLevel.INFO
            The logging level
        message: str, default ""
            The log message
        args: Tuple[Any]
            Any un-named arguments
        kwargs: Dict[str, Any]
            Any named arguments
        """

        if logging_level > LogMessage.DEBUG.value:  # If the log message is of higher priority than debug messages

            # Needed because Gtk doesn't prefer adding stuff on a different thread
            GLib.idle_add(
                lambda any_text: ( # A temporary nameless function handle to make sure that console_buffer.get_end_iter() is valid by calling it right when the insert() method is called. They are both grouped together. Using GLib.idle_add directly was somehow invalidating get_end_iter(), resulting in runtime errors, which are now fixed
                    FenixInstallerPage.console_buffer.insert(
                        FenixInstallerPage.console_buffer.get_end_iter(),
                        any_text
                    )
                ),
                "".join(("- ", message, "\n"))
            )                

class PageTools():
    """
    Handy tools for managing the installer pages

    STATIC ATTRIBUTES
    -----------------
    preloaded_pages: Optional[Dict[str, FenixInstallerPage]]
        Preloaded page objects with page names as keys
    
    """

    # STATIC VARIABLES
    preloaded_pages: Optional[Dict[str, FenixInstallerPage]] = None

    @staticmethod
    def preload_pages(
        page_configuration: JSONConfiguration
    ) -> None:
        """
        Loads all the pages within the page directory of teh configuration file
        
        Parameters
        ----------
        page_configuration: JSONConfiguration
            The installer page data

        Returns
        -------
        None
        """

        PageTools.preloaded_pages = {}

        for page_name in page_configuration["page_directory"]:
            LogMessage.Info("Preloading page: " + page_name + "...").write(logging_handler)
            page_module: ModuleType = importlib.import_module("user_interface.gtk.code" + "." + page_configuration["page_directory"][page_name]["filename"]) # dynamically import the necessary python file for the page
            page_object: FenixInstallerPage = page_module.Page() # use the corresponding python script of the page to initialize it
            page_object.set_name(page_name) # name the page for later access
            PageTools.preloaded_pages[page_name] = page_object # add the page to the list of preloaded pages
            
    @staticmethod
    def add_pages(
        list_of_pages: List[str],
        page_stack: Gtk.Stack,
        page_configuration: JSONConfiguration
    ) -> None:
        """Adds pages specified by list_of_pages to the given page stack and updates and returns the installer page data (that was originally extracted from the installer configuration file)
        
        Parameters
        ----------
        list_of_pages: List[str]
            List of page names
        page_stack: Gtk.Stack
            Widget that stores all the UI for the pages
        page_configuration: JSONConfiguration
            The installer page data
                
        Returns
        -------
        None
        """

        if PageTools.preloaded_pages is not None:
            for page_name in list_of_pages:
                if page_name in PageTools.preloaded_pages:
                    page_object: FenixInstallerPage = PageTools.preloaded_pages[page_name] # use the corresponding python script of the page to initialize it
                else:
                    LogMessage.Info("Loading page: " + page_name + "...").write(logging_handler)
                    page_module: ModuleType = importlib.import_module("user_interface.gtk.code" + "." + page_configuration["page_directory"][page_name]["filename"]) # dynamically import the necessary python file for the page
                    page_object: FenixInstallerPage = page_module.Page() # use the corresponding python script of the page to initialize it
                    page_object.set_name(page_name) # name the page for later access
                    PageTools.preloaded_pages[page_name] = page_object # add the page to the list of preloaded pages  
                page_stack.add_titled(page_object, page_name, page_configuration["page_directory"][page_name]["displayed_title"]) # add the imported page to the Gtk stack
        else:
            for page_name in list_of_pages:
                LogMessage.Info("Loading page: " + page_name + "...").write(logging_handler)
                page_module: ModuleType = importlib.import_module("user_interface.gtk.code" + "." + page_configuration["page_directory"][page_name]["filename"]) # dynamically import the necessary python file for the page
                page_object: FenixInstallerPage = page_module.Page() # use the corresponding python script of the page to initialize it
                page_object.set_name(page_name) # name the page for later access
                page_stack.add_titled(page_object, page_name, page_configuration["page_directory"][page_name]["displayed_title"]) # add the imported page to the Gtk stack

        page_configuration["added_pages"] = list_of_pages

    @staticmethod
    def remove_pages(
        list_of_pages: List[str],
        page_stack: Gtk.Stack,
        page_configuration: JSONConfiguration
    ) -> None:
        """Removes pages specified by list_of_pages from the given page stack and updates and returns the installer page data (that was originally extracted from the installer configuration file)
        
        Parameters
        ----------
        list_of_pages: List[str]
            List of page names
        page_stack: Gtk.Stack
            Widget that stores all the UI for the pages
        page_configuration: JSONConfiguration
            The installer page data
        
        Returns
        -------
        Nothing
        """
        for page in page_stack.get_children():
            if page.get_name() in list_of_pages:
                page_stack.remove(page)
        page_configuration["added_pages"] = [page_name for page_name in page_configuration["added_pages"] if page_name not in list_of_pages]

    @staticmethod
    def refresh_pages(page_stack: Gtk.Stack) -> None:
        """Forces the given page stack to update the pages shown, based on newly added or removed pages.
        
        Parameters
        ----------
        page_stack: Gtk.Stack
            Widget that stores all the UI for the pages

        Returns
        -------
        Nothing
        """
        
        page_stack.show_all()
