# Fenix Installer
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/rebornos-team/fenix/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

import numpy
import gi # Python GObject introspection module which contains Python bindings and support for Gtk
gi.require_version('Gtk', '3.0') # make sure that the Gtk version is at the required level
from gi.repository import Gtk # Gtk related modules for the graphical interface
from gi.repository import GdkPixbuf # Gtk module that describes images
from gi.repository import GLib # for using a function that will allow updating the on-screen console text

class ImageTools:
    @staticmethod
    def getPixbufFromFile(filePath: str) -> GdkPixbuf.Pixbuf:
        """
        Returns the pixel buffer from the image that is found on filePath

        Parameters
        ----------
        filePath: str
            Path to the image
        
        Returns
        -------
        GdkPixbuf.Pixbuf
            Pixel buffer from the specified image file
        """
        
        return GdkPixbuf.Pixbuf.new_from_file(filePath) # get pixbuf from an image in `filePath`

    @staticmethod
    def scalePixbufByHeight(
        pixbuf: GdkPixbuf.Pixbuf,
        desiredHeight: float,
        aspectRatio: float
    ) -> GdkPixbuf.Pixbuf:
        """
        Returns the image pixel buffer scaled to the desired height while maintaining the specified aspect ratio
        
        Parameters
        ----------
        pixbuf: GdkPixbuf.Pixbuf
            The pixel buffer to be scaled
        desiredHeight: float
            The desired height of the image pixel buffer
        aspectRatio: float
            The width to height ratio to be maintained
        
        Returns
        -------
        GdkPixbuf.Pixbuf
            Scaled image pixel buffer
        """

        # scale a given pixbuf to a specified height
        return pixbuf.scale_simple(                       
            aspectRatio * desiredHeight,
            desiredHeight,
            GdkPixbuf.InterpType.BILINEAR
        )

    @staticmethod
    def scalePixbufByWidth(
        pixbuf: GdkPixbuf.Pixbuf,
        desiredWidth: float,
        aspectRatio: float
    ) -> GdkPixbuf.Pixbuf:
        """Returns the image pixel buffer scaled to the desired width while maintaining the specified aspect ratio
        
        Parameters
        ----------
        pixbuf : GdkPixbuf.Pixbuf
            The pixel buffer to be scaled
        desiredWidth: numeric
            The desired width of the image pixel buffer
        aspectRatio: numeric
            The width to height ratio to be maintained
        
        Returns
        -------
        GdkPixbuf.Pixbuf
            Scaled image pixel buffer
        """

        return pixbuf.scale_simple(           # scale a given pixbuf to a specified width
            desiredWidth,                              
            float(desiredWidth) / aspectRatio,
            GdkPixbuf.InterpType.BILINEAR
        )    

    @staticmethod
    def addImageFromPixbuf(
        pixbuf: GdkPixbuf.Pixbuf,
        gtkImage: Gtk.Image
    ) -> None:
        """Adds a given image pixel buffer to an image widget on the GUI
        
        Parameters
        ----------
        pixbuf: GdkPixbuf.Pixbuf
            Given image pixel buffer
        gtkImage : Gtk.Image
            The widget to display the given image pixel buffer in
        """
        GLib.idle_add(gtkImage.set_from_pixbuf, pixbuf)               # add pixbuf to GtkImage in the GUI

    @staticmethod
    def addImageFromFile(filePath, desiredHeight, gtkImage):
        """Adds a specified image from a given file path to an image widget on the GUI
        
        Parameters
        ----------
        filePath : string
            The path to the image file
        desiredHeight : numeric
            The height of the image
        gtkImage : Gtk.Image
            The widget to display the image in
        """
        # TODO: Use `GdkPixbuf.Pixbuf.new_from_file_at_scale()` instead

        pixbuf = GdkPixbuf.Pixbuf.new_from_file(filePath)             # load_data image from file
        aspectRatio = float(pixbuf.get_width()) / pixbuf.get_height() # calculate the aspect ratio of the image
        pixbuf = pixbuf.scale_simple(                                 # scale the image
                aspectRatio * desiredHeight,
                desiredHeight,
                GdkPixbuf.InterpType.BILINEAR
            )
        GLib.idle_add(gtkImage.set_from_pixbuf, pixbuf)               # add the scaled image to GtkImage in the GUI

    @staticmethod
    def readPixelFromPixbuf(pixbuf, X, Y):
        """Reads the integer pixel contents at location (X,Y) in a pixel buffer
        
        Parameters
        ----------
        pixbuf : GdkPixbuf.Pixbuf
            Given image pixel buffer
        X : numeric
            The X location of the pixel to be read
        Y : Gtk.Image
            The Y location of the pixel to be read
        """
        return numpy.frombuffer(
            pixbuf.new_subpixbuf(X, Y, 1, 1).get_pixels(),
            dtype = numpy.uint8
        )


